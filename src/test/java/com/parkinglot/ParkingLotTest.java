package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingLotTest {

    @Test
    void should_return_a_ticket_when_park_a_car_given_parking_lot_and_a_car(){
        //GIVEN
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();

        //WHEN
        Ticket ticket = parkingLot.park(car);

        //THEN
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_a_parked_car_when_fetch_a_car_given_parking_lot_with_a_parked_car_and_a_ticket(){
        //GIVEN
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        Ticket ticket = parkingLot.park(car);
        //WHEN
        Car car1 = parkingLot.fetch(ticket);
        //THEN
        Assertions.assertEquals(car,car1);
    }

    @Test
    void should_return_the_right_car_when_fetch_the_cars_twice_given_parking_lot_with_two_parked_cars_and_two_tickets(){
        //GIVEN
        Car car1 = new Car();
        Car car2 = new Car();

        ParkingLot parkingLot = new ParkingLot();
        Ticket ticket1 = parkingLot.park(car1);
        Ticket ticket2 = parkingLot.park(car2);

        //WHEN

        //THEN
        Assertions.assertEquals(car1,parkingLot.fetch(ticket1));
        Assertions.assertEquals(car2,parkingLot.fetch(ticket2));
    }

    @Test
    void should_return_nothing_with_error_msg_when_fetch_the_cars_given_parking_lot_and_an_unrecognized_tickets(){
        //GIVEN
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        parkingLot.park(car);
        Ticket ticket = new Ticket();

        //THEN
        UnrecognizedException unrecognizedException = Assertions.assertThrows(UnrecognizedException.class, () -> parkingLot.fetch(ticket));

        Assertions.assertEquals("Unrecognized parking ticket.",unrecognizedException.getMessage());
    }

    @Test
    void should_return_nothing_and_error_msg_when_fetch_a_car_given_parking_lot_with__and_used_ticket(){
        //GIVEN
        Car car1 = new Car();
        ParkingLot parkingLot = new ParkingLot();
        Ticket ticket = parkingLot.park(car1);
        parkingLot.fetch(ticket);
        //THEN
        UnrecognizedException unrecognizedException = Assertions.assertThrows(UnrecognizedException.class, () -> parkingLot.fetch(ticket));

        Assertions.assertEquals("Unrecognized parking ticket.",unrecognizedException.getMessage());

    }

    @Test
    void should_return_nothing_and_error_msg_when_park_a_car_given_parking_lot_is_full_with_a_car(){
        //GIVEN
        Car car1 = new Car();
        Car car2 = new Car();

        ParkingLot parkingLot = new ParkingLot();
        parkingLot.setCapacity(1);

        parkingLot.park(car1);

        //WHEN
        NoAvailableException noAvailableException = Assertions.assertThrows(NoAvailableException.class, () -> parkingLot.park(car2));

        //THEN
        Assertions.assertEquals("No available position.",noAvailableException.getMessage());
    }
}

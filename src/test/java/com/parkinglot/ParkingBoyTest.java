package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class ParkingBoyTest {

    @Test
    void should_return_a_ticket_when_park_a_car_given_a_parking_boy_and_parking_lot_and_a_car(){
        //GIVEN
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);

        //WHEN
        Ticket ticket = parkingBoy.park(car);

        //THEN
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_a_parked_car_when_fetch_a_car_given_parking_lot_with_a_parking_boy_and_a_parked_car_and_a_ticket(){
        //GIVEN
        Car car = new Car();
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot());
        Ticket ticket = parkingBoy.park(car);
        //WHEN
        Car car1 = parkingBoy.fetch(ticket);
        //THEN
        Assertions.assertEquals(car,car1);
    }


    @Test
    void should_return_the_right_car_when_fetch_the_cars_twice_given_parking_lot_with_a_parking_boy_and_two_parked_cars_and_two_tickets(){
        //GIVEN
        Car car1 = new Car();
        Car car2 = new Car();

        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot());
        Ticket ticket1 = parkingBoy.park(car1);
        Ticket ticket2 = parkingBoy.park(car2);

        //THEN
        Assertions.assertEquals(car1,parkingBoy.fetch(ticket1));
        Assertions.assertEquals(car2,parkingBoy.fetch(ticket2));
    }

    @Test
    void should_return_nothing_and_error_msg_when_fetch_a_car_given_parking_lot_with_a_parking_boy_and_wrong_ticket(){
        //GIVEN
        Car car1 = new Car();
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot());
        parkingBoy.park(car1);
        Ticket ticket = new Ticket();

        //THEN
        UnrecognizedException unrecognizedException = Assertions.assertThrows(UnrecognizedException.class, () -> parkingBoy.fetch(ticket));

        Assertions.assertEquals("Unrecognized parking ticket.",unrecognizedException.getMessage());

    }

    @Test
    void should_return_nothing_and_error_msg_when_fetch_a_car_given_parking_lot_with_a_parking_boy_and_used_ticket(){
        //GIVEN
        Car car1 = new Car();
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot());
        Ticket ticket = parkingBoy.park(car1);
        parkingBoy.fetch(ticket);
        //THEN
        UnrecognizedException unrecognizedException = Assertions.assertThrows(UnrecognizedException.class, () -> parkingBoy.fetch(ticket));

        Assertions.assertEquals("Unrecognized parking ticket.",unrecognizedException.getMessage());

    }

    @Test
    void should_return_nothing_and_error_msg_when_park_a_car_given_parking_lot_is_full_with_a_parking_boy_and_a_car(){
        //GIVEN
        Car car1 = new Car();
        Car car2 = new Car();

        ParkingLot parkingLot = new ParkingLot();
        parkingLot.setCapacity(1);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);

        parkingBoy.park(car1);

        //WHEN
        NoAvailableException noAvailableException = Assertions.assertThrows(NoAvailableException.class, () -> parkingBoy.park(car2));

        //THEN
        Assertions.assertEquals("No available position.",noAvailableException.getMessage());
    }


    @Test
    void should_park_to_the_parking_lot_when_park_a_car_given_parking_lot_with_a_parking_boy_and_a_car(){
        //GIVEN
        ParkingLot parkingLot = new ParkingLot();
        parkingLot.setCapacity(1);
        ParkingLot parkingLot1 = new ParkingLot();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot);
        parkingLotList.add(parkingLot1);

        ParkingBoy parkingBoy = new ParkingBoy(parkingLotList);
        Car car = new Car();

        //WHEN
        Ticket ticket = parkingBoy.park(car);

        //THEN
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_park_to_the_second_parking_lot_when_park_a_car_and_first_parking_lot_is_full_given_two_parking_lots_with_a_parking_boy_and_a_car(){
        //GIVEN
        ParkingLot parkingLot = new ParkingLot();
        parkingLot.setCapacity(0);
        ParkingLot parkingLot1 = new ParkingLot();
        List<ParkingLot> parkingLotList = new ArrayList<>();

        parkingLotList.add(parkingLot);
        parkingLotList.add(parkingLot1);

        ParkingBoy parkingBoy = new ParkingBoy(parkingLotList);
        Car car = new Car();

        //WHEN
        Ticket ticket = parkingBoy.park(car);

        //THEN
        Car car1 = parkingLot1.getParkedCar().get(ticket);
        Assertions.assertEquals(car,car1);
    }

    @Test
    void should_return_the_right_car_when_fetch_cars_twice_given_two_parking_lots_with_a_parking_boy_and_two_tickets(){
        //GIVEN
        ParkingLot parkingLot = new ParkingLot();
        ParkingLot parkingLot1 = new ParkingLot();
        List<ParkingLot> parkingLotList = new ArrayList<>();

        parkingLotList.add(parkingLot);
        parkingLotList.add(parkingLot1);

        ParkingBoy parkingBoy = new ParkingBoy(parkingLotList);
        Car car1 = new Car();
        Car car2 = new Car();

        //WHEN
        Ticket ticket1 = parkingBoy.park(car1);
        Ticket ticket2 = parkingBoy.park(car2);
        Car car3 = parkingBoy.fetch(ticket1);
        Car car4 = parkingBoy.fetch(ticket2);

        //THEN
        Assertions.assertSame(car1, car3);
        Assertions.assertSame(car2, car4);
    }

    @Test
    void should_return_nothing_with_error_msg_when_fetch_given_two_parking_lots_with_a_parking_boy_and_an_unrecognized_ticket(){
        //GIVEN
        ParkingLot parkingLot = new ParkingLot();
        ParkingLot parkingLot1 = new ParkingLot();
        List<ParkingLot> parkingLotList = new ArrayList<>();

        parkingLotList.add(parkingLot);
        parkingLotList.add(parkingLot1);

        ParkingBoy parkingBoy = new ParkingBoy(parkingLotList);
        Car car = new Car();
        parkingBoy.park(car);

        Ticket ticket = new Ticket();

        //WHEN
        UnrecognizedException unrecognizedException = Assertions.assertThrows(UnrecognizedException.class, () -> parkingBoy.fetch(ticket));

        //THEN
        Assertions.assertEquals("Unrecognized parking ticket.",unrecognizedException.getMessage());
    }

    @Test
    void should_return_nothing_with_error_msg_when_fetch_given_two_parking_lots_with_a_parking_boy_and_used_ticket(){
        //GIVEN
        ParkingLot parkingLot = new ParkingLot();
        ParkingLot parkingLot1 = new ParkingLot();
        List<ParkingLot> parkingLotList = new ArrayList<>();

        parkingLotList.add(parkingLot);
        parkingLotList.add(parkingLot1);

        ParkingBoy parkingBoy = new ParkingBoy(parkingLotList);
        Car car = new Car();
        Ticket ticket = parkingBoy.park(car);

        parkingBoy.fetch(ticket);
        //WHEN
        UnrecognizedException unrecognizedException = Assertions.assertThrows(UnrecognizedException.class, () -> parkingBoy.fetch(ticket));

        //THEN
        Assertions.assertEquals("Unrecognized parking ticket.",unrecognizedException.getMessage());
    }

    @Test
    void should_return_nothing_and_error_msg_when_park_a_car_given_two_parking_lots_are_full_with_a_car(){
        //GIVEN
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        List<ParkingLot> parkingLotList = new ArrayList<>();

        parkingLot1.setCapacity(0);
        parkingLot2.setCapacity(0);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLotList);

        //WHEN
        NoAvailableException noAvailableException = Assertions.assertThrows(NoAvailableException.class, () -> parkingBoy.park(car));

        //THEN
        Assertions.assertEquals("No available position.",noAvailableException.getMessage());
    }
}
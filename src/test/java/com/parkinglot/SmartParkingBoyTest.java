package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class SmartParkingBoyTest {
    @Test
    void should_park_to_the_parking_lot_which_contains_more_empty_positions_when_park_a_car_given_parking_lot_with_a_smart_parking_boy_and_a_car(){
        //GIVEN
        ParkingLot parkingLot = new ParkingLot();
        ParkingLot parkingLot1 = new ParkingLot();
        parkingLot.setCapacity(2);
        parkingLot1.setCapacity(1);

        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot);
        parkingLotList.add(parkingLot1);

        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLotList);
        Car car = new Car();

        //WHEN
        Ticket ticket = smartParkingBoy.park(car);
        Car car1 =  parkingLot.getParkedCar().get(ticket);

        //THEN
        Assertions.assertSame(car, car1);
    }

    @Test
    void should_return_a_parked_car_when_fetch_a_car_given_parking_lot_with_a_smart_parking_boy_and_a_parked_car_and_a_ticket(){
        //GIVEN
        Car car = new Car();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(new ParkingLot());
        Ticket ticket = smartParkingBoy.park(car);
        //WHEN
        Car car1 = smartParkingBoy.fetch(ticket);
        //THEN
        Assertions.assertEquals(car,car1);
    }

    @Test
    void should_return_the_right_car_when_fetch_the_cars_twice_given_parking_lot_with_a_smart_parking_boy_and_two_parked_cars_and_two_tickets(){
        //GIVEN
        Car car1 = new Car();
        Car car2 = new Car();

        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(new ParkingLot());

        Ticket ticket1 = smartParkingBoy.park(car1);
        Ticket ticket2 = smartParkingBoy.park(car2);

        //THEN
        Assertions.assertEquals(car1,smartParkingBoy.fetch(ticket1));
        Assertions.assertEquals(car2,smartParkingBoy.fetch(ticket2));
    }

    @Test
    void should_return_nothing_and_error_msg_when_fetch_a_car_given_parking_lot_with_a_smart_parking_boy_and_wrong_ticket(){
        //GIVEN
        Car car1 = new Car();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(new ParkingLot());
        smartParkingBoy.park(car1);
        Ticket ticket = new Ticket();

        //THEN
        UnrecognizedException unrecognizedException = Assertions.assertThrows(UnrecognizedException.class, () -> smartParkingBoy.fetch(ticket));

        Assertions.assertEquals("Unrecognized parking ticket.",unrecognizedException.getMessage());

    }

    @Test
    void should_return_nothing_and_error_msg_when_fetch_a_car_given_parking_lot_with_a_smart_parking_boy_and_used_ticket(){
        //GIVEN
        Car car1 = new Car();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(new ParkingLot());

        Ticket ticket = smartParkingBoy.park(car1);
        smartParkingBoy.fetch(ticket);
        //THEN
        UnrecognizedException unrecognizedException = Assertions.assertThrows(UnrecognizedException.class, () -> smartParkingBoy.fetch(ticket));

        Assertions.assertEquals("Unrecognized parking ticket.",unrecognizedException.getMessage());

    }

    @Test
    void should_return_nothing_and_error_msg_when_park_a_car_given_parking_lot_is_full_with_a_smart_parking_boy_and_a_car(){
        //GIVEN
        Car car1 = new Car();
        Car car2 = new Car();

        ParkingLot parkingLot = new ParkingLot();
        parkingLot.setCapacity(1);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot);

        smartParkingBoy.park(car1);

        //WHEN
        NoAvailableException noAvailableException = Assertions.assertThrows(NoAvailableException.class, () -> smartParkingBoy.park(car2));

        //THEN
        Assertions.assertEquals("No available position.",noAvailableException.getMessage());
    }
}

package com.parkinglot;

import java.util.HashMap;

public class ParkingLot {
    protected HashMap<Ticket,Car> parkedCar = new HashMap<>();
    protected int capacity = 10;

    public int getCapacity() {
        return capacity;
    }

    public ParkingLot() {}

    public boolean isFull(){
        return parkedCar.size() == capacity;
    }

    public int emptyParkingSpace(){
        int notEmptyParkingSpace = parkedCar.size();
        return getCapacity() - notEmptyParkingSpace;
    }

    public double positionRate(){
        return (double) emptyParkingSpace() / getCapacity();
    }

    public Ticket park(Car car) {
        if(isFull()){
            throw new NoAvailableException();
        }
        Ticket ticket = new Ticket();
        parkedCar.put(ticket,car);
        return ticket;
    }

    public void setCapacity(int capacity){
        this.capacity = capacity;
    }
    public Car fetch(Ticket ticket) {
        if (parkedCar.containsKey(ticket)){
            Car currentCar = parkedCar.get(ticket);
            parkedCar.remove(ticket);
            return currentCar;
        } else {
            throw new UnrecognizedException();
        }
    }

    public HashMap<Ticket, Car> getParkedCar() {
        return parkedCar;
    }
}

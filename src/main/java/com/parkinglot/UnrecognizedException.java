package com.parkinglot;

public class UnrecognizedException extends RuntimeException {

    public UnrecognizedException(){
        super("Unrecognized parking ticket.");
    }

}

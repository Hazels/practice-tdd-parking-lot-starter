package com.parkinglot;

import java.util.List;

public class SmartParkingBoy extends ParkingBoy {


    public SmartParkingBoy(List<ParkingLot> parkingLots) {
        super(parkingLots);
    }

    public SmartParkingBoy(ParkingLot parkingLot) {
        super(parkingLot);
    }

    public Ticket park(Car car) {
        return this.getBiggerParkingLot().park(car);
    }

    public ParkingLot getBiggerParkingLot() {
        return parkingLots.stream().reduce((max, parkingLot) ->
                        max.emptyParkingSpace() > parkingLot.emptyParkingSpace() ? max : parkingLot).get();
    }
}

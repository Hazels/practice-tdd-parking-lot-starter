package com.parkinglot;

import java.util.List;

public class SuperSmartParkingBoy extends ParkingBoy {
    public SuperSmartParkingBoy(ParkingLot parkingLot) {
        super(parkingLot);
    }

    public SuperSmartParkingBoy(List<ParkingLot> parkingLots) {
        super(parkingLots);
    }
    public Ticket park(Car car) {
        return this.getLargerParkingLot().park(car);
    }

    public ParkingLot getLargerParkingLot() {
        return parkingLots.stream().reduce((max, parkingLot) ->
                max.positionRate() <= parkingLot.positionRate() ? max : parkingLot).get();
    }
}

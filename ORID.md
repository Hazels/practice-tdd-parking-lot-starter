# Daily Report (2023/07/14)
# O (Objective): 
## 1. What did we learn today?
### We learned command pattern,unit testing and TDD(Test Driven Development) today.

## 2. What activities did you do? 
### I had a code review with my team members to make sure the code was compliant,played a game and imply the parking lot with many stories.

## 3. What scenes have impressed you?
### I have been impressed by the code review with my team members and the stories of the parking lot today .

---------------------

# R (Reflective): 
## Please use one word to express your feelings about today's class.
---------------------
### stimulating.

# I (Interpretive):
## What do you think about this? What was the most meaningful aspect of this activity?
---------------------
### I think the most meaningful aspect of this activity is implying demand iteration for the parking lot, because I I have learned how to complete unit testing for functional iterations write code that is easy to maintain.


# D (Decisional): 
## Where do you most want to apply what you have learned today? What changes will you make?
---------------------
### I will use Java stream instead of loo and try to reduce the use of if else statements as much as possible when I write the functions.And I try to apply design patterns to code development.
### I will develop a good coding habit of committing in BB steps.




